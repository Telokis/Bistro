#include <iostream>
#include <map>
#include <functional>
#include <string>
#include <stack>
#include <iomanip>
#include "Bistro/Engine.hh"

using NumberType_t = int;

#define TEST(expr, expected)                                                            \
  {                                                                                     \
    engine.setInput(expr);                                                              \
    auto val = engine.calc();                                                           \
    std::cout << std::setw(20) << std::right << engine.getInput() << " | " << std::left \
              << std::setw(5) << val << " | " << (val == (expected)) << "\n";           \
  }

int main()
{
  Bistro::Engine<NumberType_t> engine;

  engine.registerFunction("abs", [](const NumberType_t& val) { return std::abs(val); });
  engine.registerFunction(
    "min", [](const NumberType_t& a, const NumberType_t& b) { return std::min(a, b); });
  engine.registerFunction(
    "max", [](const NumberType_t& a, const NumberType_t& b) { return std::max(a, b); });

  engine.registerToken<Bistro::FaketorialToken>();

  /* clang-format off */
  TEST("1 + -2 * 3 + -1", -6)
  TEST("7 - 4 + -28 * 2", -53)
  TEST("1 + 1 * 2", 3)
  TEST("128 / 4 * -(2 + 2)", -128)
  TEST("-((1 + 1) + -1) * 2", -2)
  TEST("(2 + 2) - 1", 3)
  TEST("2 ** 4", 16)
  TEST("(1)", 1)
  TEST("-3", -3)
  TEST("3!", 6)
  TEST("3! + (-2*-3*-4)", -18)
  TEST("3! + -2!", -18)
  TEST("-1!", -6)
  TEST("3!!", 120)
  TEST("abs(-4)!", 24)
  TEST("abs(-abs(-4))", 4)
  TEST("abs(-2)+-abs(-3*2)", -4)
  TEST("min(1,2)", 1)
  TEST("max(-18, min(7, 24))", 7)
  /* clang-format on */

  std::cout << "Finished\n";

  return 0;
}
