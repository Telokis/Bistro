#! /bin/sh

run() {
    echo "> $*"
    $*
}

if [ "$1" = "build" ]; then
    if [ "$#" -eq 1 ]; then
        run sw -j8 -config r --build-name bistro build -output-dir bin
    else
        run sw -j8 -config r --build-name bistro build -output-dir bin -target bistro.$2-0.0.1
    fi
elif [ "$1" = "debug" ]; then
    if [ "$#" -eq 1 ]; then
        run sw -j8 -config d --build-name bistro build -output-dir .sw/out/debug
    else
        run sw -j8 -config d --build-name bistro build -output-dir .sw/out/debug -target bistro.$2-0.0.1
    fi
elif [ "$1" = "generate" ]; then
    run sw -j8 -config r --build-name bistro generate
elif [ "$1" = "compdb" ]; then
    run sw -j8 --build-name bistro generate -g compdb
elif [ "$1" = "run" ]; then
    run ./bin/bistro-0.0.1.exe "${@:3}"
else
    echo "Command not found: $1"
    echo "Try build, debug, run, generate or compdb"
fi