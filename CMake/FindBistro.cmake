# Try to find the Bistro library
#
# This module defines the following variables:
# 
# BISTRO_FOUND            - If Bistro was found
# BISTRO_INCLUDE_DIR      - The include directory of Bistro
# BISTRO_LIBRARY          - Link against this variable
# BISTRO_LIBRARY_DEBUG    - Contains the debug library if found. Otherwise, contains the release.
# BISTRO_LIBRARY_RELEASE  - Contains the release library if found. Otherwise, contains the debug.
# 
# This module will use the following variables:
# 
# BISTRO_ROOT - This will be used to find Bistro
#

# Defines potential paths for finding Bistro
SET(BISTRO_FIND_PATHS
  ${BISTRO_ROOT}
  "C:/Program Files (x86)/bistro"
  "C:/Program Files/bistro"
  $ENV{BISTRO_ROOT}
  /usr/local/
  /usr/
  /sw
  /opt/local/
  /opt/csw/
  /opt/
)

if (NOT BISTRO_FIND_QUIETLY)
  message(STATUS "Looking for Bistro...")
endif ()

# Look for include folder
find_path(BISTRO_INCLUDE_DIR NAMES Bistro/Engine.hh
  HINTS
    ${BISTRO_FIND_PATHS}
  PATH_SUFFIXES include)

# Look for debug library
find_library(BISTRO_LIBRARY_DEBUG
  NAMES         bistro_d
  PATH_SUFFIXES lib64 lib
  PATHS         ${BISTRO_FIND_PATHS})

# Look for release library
find_library(BISTRO_LIBRARY_RELEASE
  NAMES         bistro
  PATH_SUFFIXES lib64 lib
  PATHS         ${BISTRO_FIND_PATHS})

# If at least one library was found
if (BISTRO_LIBRARY_DEBUG OR BISTRO_LIBRARY_RELEASE)

  # Mark as found
  SET(BISTRO_FOUND TRUE)

  if (BISTRO_LIBRARY_DEBUG AND BISTRO_LIBRARY_RELEASE)
    # Both libraries were found
    SET(BISTRO_LIBRARY
      debug     ${BISTRO_LIBRARY_DEBUG}
      optimized ${BISTRO_LIBRARY_RELEASE})

  elseif(BISTRO_LIBRARY_DEBUG)
    # Only debug version was found
    SET(BISTRO_LIBRARY ${BISTRO_LIBRARY_DEBUG})
    SET(BISTRO_LIBRARY_RELEASE ${BISTRO_LIBRARY_DEBUG})

  elseif(BISTRO_LIBRARY_RELEASE)
    # Only release version was found
    SET(BISTRO_LIBRARY ${BISTRO_LIBRARY_RELEASE})
    SET(BISTRO_LIBRARY_DEBUG ${BISTRO_LIBRARY_RELEASE})
  endif()
else()
  # Bistro was not found
  SET(BISTRO_FOUND FALSE)
endif()

# Don't show variables to user
mark_as_advanced(
  BISTRO_INCLUDE_DIR
  BISTRO_LIBRARY
  BISTRO_LIBRARY_RELEASE
  BISTRO_LIBRARY_DEBUG
)

if (BISTRO_FOUND)
  message(STATUS "-- Found Bistro : ${BISTRO_LIBRARY}")
else()
  if(BISTRO_FIND_REQUIRED)
    # Fatal error
    message(FATAL_ERROR "Could NOT find Bistro")
  elseif(NOT BISTRO_FIND_QUIETLY)
    # Error, but continue
    message("Could NOT find Bistro")
  endif()
endif()