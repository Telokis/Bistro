function(prefix_list output_result input_prefix input_list)
  foreach(val IN LISTS ${input_list})
    list(APPEND FUNCTION_RESULT "${input_prefix}${val}")
  endforeach(val)
  set(${output_result} ${FUNCTION_RESULT} PARENT_SCOPE)
endfunction(prefix_list)

# Defines a macro to ease the creation of new tests
macro(create_test TestName)
  cmake_parse_arguments(BISTRO_TESTING "" "NAMESPACE;LIBRARY;PREFIX" "FILES;INCLUDE;DEPENDENCIES" ${ARGN})

  set(FINAL_TEST_NAME TEST_${BISTRO_TESTING_NAMESPACE}_${TestName})

  prefix_list(PREFIXED_SOURCES ${BISTRO_TESTING_PREFIX} BISTRO_TESTING_FILES)

  # message("----------------------DEBUG---------------------")
  # message("NAMESPACE: ${BISTRO_TESTING_NAMESPACE}")
  # message("LIBRARY: ${BISTRO_TESTING_LIBRARY}")
  # message("PREFIX: ${BISTRO_TESTING_PREFIX}")
  # message("FILES: ${BISTRO_TESTING_FILES}")
  # message("INCLUDE: ${BISTRO_TESTING_INCLUDE}")
  # message("DEPENDENCIES: ${BISTRO_TESTING_DEPENDENCIES}")
  # message("PREFIXED: ${PREFIXED_SOURCES}")
  # message("------------------------------------------------")

  add_executable(${FINAL_TEST_NAME} ${PREFIXED_SOURCES})
  target_link_libraries(
    ${FINAL_TEST_NAME}
    ${BISTRO_TESTING_LIBRARY}
    ${BISTRO_TESTING_DEPENDENCIES}
    gtest
    gtest_main
  )

  target_include_directories(${FINAL_TEST_NAME} PRIVATE ${BISTRO_TESTING_INCLUDE})
  add_test(NAME ${FINAL_TEST_NAME} COMMAND ${FINAL_TEST_NAME})
endmacro()