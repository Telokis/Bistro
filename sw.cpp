void build(Solution& s)
{
  auto& bistro = s.addTarget<StaticLibraryTarget>("bistro", "0.0.1");

  bistro.CPPVersion        = CPPLanguageStandard::CPP17;
  bistro.HeaderOnly        = true;
  bistro.AutoDetectOptions = false;

  bistro.Private += "include/.*"_rr;
  bistro.Public += "include"_id;

  {
    auto& tester      = s.addTarget<ExecutableTarget>("bistro");
    tester.CPPVersion = CPPLanguageStandard::CPP17;

    // Files
    tester.Private += "src/.*"_rr;

    // Dependencies
    tester.Private += bistro;
  }
}

// sw upload pub.telokis