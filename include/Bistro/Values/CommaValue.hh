#pragma once

#ifndef BISTRO_VALUES_COMMA_VALUE_HH_
#define BISTRO_VALUES_COMMA_VALUE_HH_

#include "Bistro/Values/AUnaryOperatorValue.hh"

namespace Bistro
{
  template <class T>
  class CommaValue : public AUnaryOperatorValue<T>
  {
  public:
    CommaValue();
    T             process(const std::vector<T>& args) const override;
    std::ostream& print(std::ostream& os) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  CommaValue<T>::CommaValue() : AUnaryOperatorValue(1, Associativity::Right)
  {
  }

  template <class T>
  T CommaValue<T>::process(const std::vector<T>& args) const
  {
    return args.front();
  }

  template <class T>
  std::ostream& CommaValue<T>::print(std::ostream& os) const
  {
    return os << "Comma";
  }
}  // namespace Bistro

#endif  // BISTRO_VALUES_COMMA_VALUE_HH_