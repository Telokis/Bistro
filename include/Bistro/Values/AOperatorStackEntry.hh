#pragma once

#ifndef BISTRO_VALUES_AOPERATORSTACKENTRY_HH_
#define BISTRO_VALUES_AOPERATORSTACKENTRY_HH_

#include "Bistro/IValue.hh"
#include "Bistro/Associativity.hh"

namespace Bistro
{
  template <class T>
  class AOperatorStackEntry : public IValue<T>
  {
  public:
    virtual std::size_t   getPrecedence() const    = 0;
    virtual Associativity getAssociativity() const = 0;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
}  // namespace Bistro

#endif  // BISTRO_VALUES_AOPERATORSTACKENTRY_HH_