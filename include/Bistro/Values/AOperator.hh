#pragma once

#ifndef BISTRO_VALUES_AOPERATOR_HH_
#define BISTRO_VALUES_AOPERATOR_HH_

#include "Bistro/Values/AOperatorStackEntry.hh"

namespace Bistro
{
  template <class T, size_t Arity>
  class AOperator : public AOperatorStackEntry<T>
  {
    static_assert(Arity == 1 || Arity == 2, "Arity must be 1 or 2.");

  public:
    AOperator(std::size_t precedence, Associativity associativity);

    std::size_t   getArgumentsNumber() const override;
    std::size_t   getPrecedence() const override;
    Associativity getAssociativity() const override;
    TokenType     getType() const override;

  protected:
    const std::size_t   _precedence;
    const Associativity _associativity;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T, size_t Arity>
  AOperator<T, Arity>::AOperator(std::size_t precedence, Associativity associativity)
    : _precedence(precedence), _associativity(associativity)
  {
  }

  template <class T, size_t Arity>
  std::size_t AOperator<T, Arity>::getArgumentsNumber() const
  {
    return Arity;
  }

  template <class T, size_t Arity>
  std::size_t AOperator<T, Arity>::getPrecedence() const
  {
    return _precedence;
  }

  template <class T, size_t Arity>
  Associativity AOperator<T, Arity>::getAssociativity() const
  {
    return _associativity;
  }

  template <class T, size_t Arity>
  TokenType AOperator<T, Arity>::getType() const
  {
    return TokenType::Operator;
  }
}  // namespace Bistro

#endif  // BISTRO_VALUES_AOPERATOR_HH_