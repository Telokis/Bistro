#pragma once

#ifndef BISTRO_VALUES_NUMBERVALUE_HH_
#define BISTRO_VALUES_NUMBERVALUE_HH_

#include "Bistro/IValue.hh"

namespace Bistro
{
  template <class T>
  class NumberValue : public IValue<T>
  {
  public:
    NumberValue(T value);

    std::size_t   getArgumentsNumber() const override;
    T             process(const std::vector<T>&) const override;
    TokenType     getType() const override;
    std::ostream& print(std::ostream& os) const override;

  private:
    T _value;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  NumberValue<T>::NumberValue(T value) : _value(value)
  {
  }

  template <class T>
  std::size_t NumberValue<T>::getArgumentsNumber() const
  {
    return 0;
  }

  template <class T>
  T NumberValue<T>::process(const std::vector<T>&) const
  {
    return _value;
  }

  template <class T>
  TokenType NumberValue<T>::getType() const
  {
    return TokenType::Number;
  }

  template <class T>
  std::ostream& NumberValue<T>::print(std::ostream& os) const
  {
    return os << "Number(\"" << _value << "\")";
  }
}  // namespace Bistro

#endif  // BISTRO_VALUES_NUMBERVALUE_HH_