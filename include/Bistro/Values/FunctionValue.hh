#pragma once

#ifndef BISTRO_VALUES_FUNCTIONVALUE_HH_
#define BISTRO_VALUES_FUNCTIONVALUE_HH_

#include <functional>
#include "Bistro/Values/AOperatorStackEntry.hh"

namespace Bistro
{
  template <class T>
  class FunctionValue : public AOperatorStackEntry<T>
  {
  public:
    using HandlerType = std::function<T(const std::vector<T>& args)>;

  public:
    FunctionValue(HandlerType handler, size_t argsCount);

    std::size_t   getArgumentsNumber() const override;
    T             process(const std::vector<T>&) const override;
    TokenType     getType() const override;
    std::size_t   getPrecedence() const override;
    Associativity getAssociativity() const override;
    std::ostream& print(std::ostream& os) const override;

  private:
    HandlerType _handler;
    size_t      _argsCount;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  FunctionValue<T>::FunctionValue(HandlerType handler, size_t argsCount)
    : _handler(handler), _argsCount(argsCount)
  {
  }

  template <class T>
  std::size_t FunctionValue<T>::getArgumentsNumber() const
  {
    return _argsCount;
  }

  template <class T>
  T FunctionValue<T>::process(const std::vector<T>& args) const
  {
    return _handler(args);
  }

  template <class T>
  TokenType FunctionValue<T>::getType() const
  {
    return TokenType::Function;
  }

  template <class T>
  std::size_t FunctionValue<T>::getPrecedence() const
  {
    return 5000;
  }

  template <class T>
  Associativity FunctionValue<T>::getAssociativity() const
  {
    return Associativity::Right;
  }

  template <class T>
  std::ostream& FunctionValue<T>::print(std::ostream& os) const
  {
    return os << "Function";
  }
}  // namespace Bistro

#endif  // BISTRO_VALUES_FUNCTIONVALUE_HH_