#pragma once

#ifndef BISTRO_VALUES_LEFT_PARENTHESIS_VALUE_HH_
#define BISTRO_VALUES_LEFT_PARENTHESIS_VALUE_HH_

#include "Bistro/Values/AOperatorStackEntry.hh"

namespace Bistro
{
  template <class T>
  class LeftParenthesisValue : public AOperatorStackEntry<T>
  {
  public:
    LeftParenthesisValue();

    std::size_t   getArgumentsNumber() const override;
    std::size_t   getPrecedence() const override;
    Associativity getAssociativity() const override;
    T             process(const std::vector<T>& args) const override;
    TokenType     getType() const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

#include <stdexcept>

namespace Bistro
{
  template <class T>
  LeftParenthesisValue<T>::LeftParenthesisValue()
  {
  }

  template <class T>
  std::size_t LeftParenthesisValue<T>::getArgumentsNumber() const
  {
    return 0;
  }

  template <class T>
  std::size_t LeftParenthesisValue<T>::getPrecedence() const
  {
    return 5000;
  }

  template <class T>
  Associativity LeftParenthesisValue<T>::getAssociativity() const
  {
    return Associativity::Right;
  }

  template <class T>
  T LeftParenthesisValue<T>::process(const std::vector<T>& args) const
  {
    throw std::logic_error("WTF");
  }

  template <class T>
  TokenType LeftParenthesisValue<T>::getType() const
  {
    return TokenType::LeftParenthesis;
  }
}  // namespace Bistro

#endif  // BISTRO_VALUES_LEFT_PARENTHESIS_VALUE_HH_