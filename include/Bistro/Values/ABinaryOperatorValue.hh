#pragma once

#ifndef BISTRO_VALUES_ABINARY_OPERATOR_VALUE_HH_
#define BISTRO_VALUES_ABINARY_OPERATOR_VALUE_HH_

#include "Bistro/Values/AOperator.hh"

namespace Bistro
{
  template <class T>
  class ABinaryOperatorValue : public AOperator<T, 2>
  {
  public:
    ABinaryOperatorValue(std::size_t precedence, Associativity associativity);
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  ABinaryOperatorValue<T>::ABinaryOperatorValue(std::size_t precedence, Associativity associativity)
    : AOperator(precedence, associativity)
  {
  }
}  // namespace Bistro

#define BISTRO_MAKE_BINARY_OPERATOR_VALUE(name, precedence, associativity, handler)              \
  template <class T>                                                                             \
  class name##Value : public ABinaryOperatorValue<T>                                             \
  {                                                                                              \
  public:                                                                                        \
    name##Value();                                                                               \
    T             process(const std::vector<T>& args) const override;                            \
    std::ostream& print(std::ostream& os) const override;                                        \
  };                                                                                             \
                                                                                                 \
  template <class T>                                                                             \
  name##Value<T>::name##Value() : ABinaryOperatorValue(precedence, Associativity::associativity) \
  {                                                                                              \
  }                                                                                              \
                                                                                                 \
  template <class T>                                                                             \
  T name##Value<T>::process(const std::vector<T>& args) const                                    \
  {                                                                                              \
    auto&& left  = args.front();                                                                 \
    auto&& right = args.back();                                                                  \
    return (handler);                                                                            \
  }                                                                                              \
  template <class T>                                                                             \
  std::ostream& name##Value<T>::print(std::ostream& os) const                                    \
  {                                                                                              \
    return os << #name;                                                                          \
  }

#endif  // BISTRO_VALUES_ABINARY_OPERATOR_VALUE_HH_