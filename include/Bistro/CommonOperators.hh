#pragma once

#ifndef BISTRO_VALUES_COMMON_OPERATORS_HH_
#define BISTRO_VALUES_COMMON_OPERATORS_HH_

#include "Bistro/Values/ABinaryOperatorValue.hh"
#include "Bistro/Values/AUnaryOperatorValue.hh"

#include "Bistro/Tokens/ABinaryOperatorToken.hh"
#include "Bistro/Tokens/AUnaryOperatorToken.hh"

#define BISTRO_MAKE_BINARY_OPERATOR(name, op, precedence, associativity, handler) \
  BISTRO_MAKE_BINARY_OPERATOR_VALUE(name, precedence, associativity, handler);    \
  BISTRO_MAKE_BINARY_OPERATOR_TOKEN(name, op);

#define BISTRO_MAKE_UNARY_OPERATOR(name, op, precedence, associativity, handler) \
  BISTRO_MAKE_UNARY_OPERATOR_VALUE(name, precedence, associativity, handler);    \
  BISTRO_MAKE_UNARY_OPERATOR_TOKEN(name, op, associativity);

namespace Bistro
{
  // Precedence is based on
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
  // Higher precedence means higher priority.

  /* clang-format off */
  BISTRO_MAKE_BINARY_OPERATOR(Addition,        "+",   14,   Left, left + right);
  BISTRO_MAKE_BINARY_OPERATOR(Subtraction,     "-",   14,   Left, left - right);
  BISTRO_MAKE_BINARY_OPERATOR(Division,        "/",   15,   Left, left / right);
  BISTRO_MAKE_BINARY_OPERATOR(Multiplication,  "*",   15,   Left, left * right);
  BISTRO_MAKE_BINARY_OPERATOR(Exponentiation, "**",   16,  Right, std::pow(left, right));

  BISTRO_MAKE_UNARY_OPERATOR(Faketorial, "!",   17,   Left, val * (val - 1) * (val - 2));
  BISTRO_MAKE_UNARY_OPERATOR(Negate,     "-",   18,  Right, -val);
  /* clang-format on */
}  // namespace Bistro

#endif  // BISTRO_VALUES_COMMON_OPERATORS_HH_