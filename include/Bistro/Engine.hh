#pragma once

#ifndef BISTRO_ENGINE_HH_
#define BISTRO_ENGINE_HH_

#include <memory>
#include <string>
#include <deque>
#include "Bistro/Tokens/NumberToken.hh"
#include "Bistro/CommonOperators.hh"
#include "Bistro/Tokens/LeftParenthesisToken.hh"
#include "Bistro/Tokens/RightParenthesisToken.hh"
#include "Bistro/Tokens/FunctionToken.hh"
#include "Bistro/Tokens/CommaToken.hh"
#include "Bistro/CachedValue.hh"

namespace Bistro
{
  template <class T>
  class Engine
  {
  public:
    using NumberType                 = T;
    using ResultContainerType        = std::deque<std::unique_ptr<IValue<NumberType>>>;
    using OperatorsContainerType     = std::deque<std::unique_ptr<AOperatorStackEntry<NumberType>>>;
    using ITokenType                 = std::unique_ptr<IToken<NumberType>>;
    using TokenCollectionType        = std::deque<ITokenType>;
    using IValueType                 = std::unique_ptr<IValue<NumberType>>;
    using PreprocessedCollectionType = ResultContainerType;
    using OperatorsCollectionType    = OperatorsContainerType;

  public:
    explicit Engine(bool registerDefaultTokens = true);
    ~Engine();

  public:
    template <template <typename NumType> typename TokenType, class... Args>
    void registerToken(Args&&... args);

    template <class... Args>
    void registerFunction(Args&&... args);

  public:
    void        setInput(const std::string& input, bool doPreprocessing = true);
    NumberType  calc();
    void        preprocess();
    std::string getInput() const;
    bool        isValueToken(const ITokenType& token, const IValueType& value) const;

  protected:
    TokenCollectionType        _tokens;
    PreprocessedCollectionType _preprocessedData;
    OperatorsCollectionType    _operators;
    CachedValue<NumberType>    _finalResult;
    CachedValue<bool>          _isInputValid;
    std::string                _input;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

#include <algorithm>
#include <list>

namespace Bistro
{
  template <class T>
  Engine<T>::Engine(bool registerDefaultTokens) : _input("")
  {
    if (registerDefaultTokens)
    {
      registerToken<NumberToken>();
      registerToken<AdditionToken>();
      registerToken<SubtractionToken>();
      registerToken<DivisionToken>();
      registerToken<MultiplicationToken>();
      registerToken<NegateToken>();
      registerToken<CommaToken>();
      registerToken<RightParenthesisToken>();
      registerToken<LeftParenthesisToken>();
      registerToken<ExponentiationToken>();
    }
  }

  template <class T>
  Engine<T>::~Engine()
  {
  }

  template <class T>
  template <template <typename NumType> typename TokenType, class... Args>
  void Engine<T>::registerToken(Args&&... args)
  {
    auto token = std::make_unique<TokenType<NumberType>>(std::forward<Args>(args)...);

    auto position = std::lower_bound(
      _tokens.begin(), _tokens.end(), token->getMinimumLength(), [](auto&& a, std::size_t len) {
        return len < a->getMinimumLength();
      });

    _tokens.insert(position, std::move(token));
  }

  template <class T>
  template <class... Args>
  void Engine<T>::registerFunction(Args&&... args)
  {
    registerToken<FunctionToken>(std::forward<Args>(args)...);
  }

  template <class T>
  void Engine<T>::setInput(const std::string& input, bool doPreprocessing)
  {
    _preprocessedData.clear();
    _operators.clear();
    _finalResult = 0;
    _finalResult.invalidate();
    _input = input;
    _isInputValid.invalidate();

    if (doPreprocessing)
      preprocess();
  }

  template <class T>
  auto Engine<T>::calc() -> NumberType
  {
    if (_finalResult.isValid())
      return _finalResult();

    if (!_isInputValid.isValid())
      preprocess();

    if (!*_isInputValid)
    {
      std::cerr << "The expression is invalid. See error above.\n";
      return 1;
    }

    std::list<NumberType> result_list;
    while (!_preprocessedData.empty())
    {
      IValueType& val    = _preprocessedData.front();
      std::size_t nbArgs = val->getArgumentsNumber();

      if (nbArgs > result_list.size())
      {
        std::cerr << "Badly formatted : Expected " << nbArgs << " on stack but only "
                  << result_list.size() << " left.\n";
        return 1;
      }

      std::vector<NumberType> args;
      auto                    it = result_list.cend();

      std::advance(it, -static_cast<int>(nbArgs));
      args.insert(args.end(), it, result_list.cend());
      result_list.erase(it, result_list.cend());

      result_list.push_back(val->process(args));

      _preprocessedData.pop_front();
    }

    if (result_list.size() != 1)
    {
      std::cerr << "Badly formatted : " << result_list.size() << " numbers left. Expected 1.\n";
      return 1;
    }

    _finalResult = result_list.front();
    return _finalResult();
  }

  template <class T>
  void Engine<T>::preprocess()
  {
    std::size_t i                 = 0;
    std::size_t max               = _input.size();
    bool        found             = false;
    bool        lastTokenWasValue = false;

    while (i < max)
    {
      found = false;
      if (_input.at(i) == ' ' || _input.at(i) == '\t')
        ++i;
      else
      {
        for (auto it = _tokens.begin(); i < max && it != _tokens.end(); ++it)
        {
          ITokenType& token = *it;

          if (token->test(_input, i, lastTokenWasValue))
          {
            std::size_t len = 0;

            IValueType value  = token->parse(_input, i, len);
            lastTokenWasValue = isValueToken(token, value);
            bool success      = token->handle(_preprocessedData, _operators, value);

            if (!success)
            {
              std::cerr << "Preprocessing failed.\n";
              _isInputValid = false;
              return;
            }

            i += len;
            found = true;
          }
        }

        if (!found)
        {
          std::cerr << "Unknown token : " << _input.at(i) << "\n";
          ++i;
        }
      }
    }

    for (auto&& it = _operators.rbegin(); it != _operators.rend(); ++it)
    {
      if ((*it)->getType() == TokenType::LeftParenthesis)
      {
        std::cerr << "Found a LeftParenthesis on the operator stack. Ensure you have matching "
                     "parenthesis.\n";
        _isInputValid = false;
        return;
      }

      _preprocessedData.push_back(std::move(*it));
    }

// Print stack for debugging
#if 0
    for (auto&& val : _preprocessedData)
    {
      val->print(std::cout) << "\n";
    }
#endif

    _isInputValid = true;
  }

  template <class T>
  bool Engine<T>::isValueToken(const ITokenType& token, const IValueType& value) const
  {
    auto tokenType = token->getType();

    if (tokenType == TokenType::RightParenthesis || tokenType == TokenType::Number
        || tokenType == TokenType::Variable)
    {
      return true;
    }

    if (token->getType() == TokenType::Operator)
    {
      auto casted = dynamic_cast<AOperator<T, 1>*>(value.get());

      if (casted && casted->getAssociativity() == Associativity::Left)
      {
        return true;
      }
    }

    return false;
  }

  template <class T>
  std::string Engine<T>::getInput() const
  {
    return _input;
  }
}  // namespace Bistro

#endif  // BISTRO_ENGINE_HH_