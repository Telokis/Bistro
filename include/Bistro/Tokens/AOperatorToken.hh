#pragma once

#ifndef BISTRO_TOKENS_AOPERATOR_TOKEN_HH_
#define BISTRO_TOKENS_AOPERATOR_TOKEN_HH_

#include "Bistro/IToken.hh"

namespace Bistro
{
  template <class T, class OperatorValue>
  class AOperatorToken : public IToken<T>
  {
  public:
    AOperatorToken(const std::string& strToMatch);

  public:
    std::size_t getMinimumLength() const override;

    TokenType getType() const override;

    ValuePtrType parse(const std::string& input,
                       std::size_t        index,
                       std::size_t&       len) const override;

    bool handle(ResultContainerType<T>&    result,
                OperatorsContainerType<T>& operators,
                ValueType<T>&              currentValue) const override;

  protected:
    const std::string _strToMatch;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T, class OperatorValue>
  AOperatorToken<T, OperatorValue>::AOperatorToken(const std::string& strToMatch)
    : _strToMatch(strToMatch)
  {
  }

  template <class T, class OperatorValue>
  std::size_t AOperatorToken<T, OperatorValue>::getMinimumLength() const
  {
    return _strToMatch.size();
  }

  template <class T, class OperatorValue>
  TokenType AOperatorToken<T, OperatorValue>::getType() const
  {
    return TokenType::Operator;
  }

  template <class T, class OperatorValue>
  bool AOperatorToken<T, OperatorValue>::handle(ResultContainerType<T>&    result,
                                                OperatorsContainerType<T>& operators,
                                                ValueType<T>&              value) const
  {
    Bistro::Handlers::simpleOperator(result, operators, value);
    return true;
  }

  template <class T, class OperatorValue>
  auto AOperatorToken<T, OperatorValue>::parse(const std::string& input,
                                               std::size_t        index,
                                               std::size_t&       len) const -> ValuePtrType
  {
    len = _strToMatch.size();
    return ValuePtrType(new OperatorValue);
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_AOPERATOR_TOKEN_HH_