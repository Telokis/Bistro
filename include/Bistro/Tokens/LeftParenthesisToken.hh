#pragma once

#ifndef BISTRO_TOKENS_LEFT_PARENTHESIS_TOKEN_HH_
#define BISTRO_TOKENS_LEFT_PARENTHESIS_TOKEN_HH_

#include "Bistro/IToken.hh"

namespace Bistro
{
  template <class T>
  class LeftParenthesisToken : public IToken<T>
  {
  public:
    LeftParenthesisToken();

  public:
    std::size_t getMinimumLength() const override;

    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;

    TokenType getType() const override;

    ValuePtrType parse(const std::string& input,
                       std::size_t        index,
                       std::size_t&       len) const override;

    bool handle(ResultContainerType<T>&    result,
                OperatorsContainerType<T>& operators,
                ValueType<T>&              currentValue) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

#include "Bistro/Values/LeftParenthesisValue.hh"

namespace Bistro
{
  template <class T>
  LeftParenthesisToken<T>::LeftParenthesisToken()
  {
  }

  template <class T>
  std::size_t LeftParenthesisToken<T>::getMinimumLength() const
  {
    return 1;
  }

  template <class T>
  bool LeftParenthesisToken<T>::test(const std::string& input,
                                     std::size_t        index,
                                     bool               lastTokenWasValue) const
  {
    return input.at(index) == '(';
  }

  template <class T>
  TokenType LeftParenthesisToken<T>::getType() const
  {
    return TokenType::LeftParenthesis;
  }

  template <class T>
  bool LeftParenthesisToken<T>::handle(ResultContainerType<T>&,
                                       OperatorsContainerType<T>& operators,
                                       ValueType<T>&              value) const
  {
    std::unique_ptr<AOperatorStackEntry<T>> ptr(
      static_cast<AOperatorStackEntry<T>*>(value.release()));

    operators.push_back(std::move(ptr));
    return true;
  }

  template <class T>
  auto LeftParenthesisToken<T>::parse(const std::string& input,
                                      std::size_t        index,
                                      std::size_t&       len) const -> ValuePtrType
  {
    len = 1;
    return std::make_unique<LeftParenthesisValue<T>>();
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_LEFT_PARENTHESIS_TOKEN_HH_