#pragma once

#ifndef BISTRO_TOKENS_COMMA_TOKEN_HH_
#define BISTRO_TOKENS_COMMA_TOKEN_HH_

#include "Bistro/IToken.hh"
#include "Bistro/Tokens/AUnaryOperatorToken.hh"
#include "Bistro/Values/CommaValue.hh"

namespace Bistro
{
  template <class T>
  class CommaToken : public AUnaryOperatorToken<T, CommaValue<T>>
  {
  public:
    CommaToken();

  public:
    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  CommaToken<T>::CommaToken() : AUnaryOperatorToken(",", Associativity::Right)
  {
  }

  template <class T>
  bool CommaToken<T>::test(const std::string& input,
                           std::size_t        index,
                           bool               lastTokenWasValue) const
  {
    if (!lastTokenWasValue)
      return false;

    return input.compare(index, _strToMatch.size(), _strToMatch) == 0;
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_COMMA_TOKEN_HH_