#pragma once

#ifndef BISTRO_TOKENS_AUNARY_OPERATOR_TOKEN_HH_
#define BISTRO_TOKENS_AUNARY_OPERATOR_TOKEN_HH_

#include "Bistro/IToken.hh"
#include "Bistro/Tokens/AOperatorToken.hh"

namespace Bistro
{
  template <class T, class UnaryOperatorValue>
  class AUnaryOperatorToken : public AOperatorToken<T, UnaryOperatorValue>
  {
  public:
    AUnaryOperatorToken(const std::string& strToMatch, Associativity associativity);

  public:
    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;

  protected:
    const Associativity _associativity;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T, class UnaryOperatorValue>
  AUnaryOperatorToken<T, UnaryOperatorValue>::AUnaryOperatorToken(const std::string& strToMatch,
                                                                  Associativity      associativity)
    : AOperatorToken(strToMatch), _associativity(associativity)
  {
  }

  template <class T, class UnaryOperatorValue>
  bool AUnaryOperatorToken<T, UnaryOperatorValue>::test(const std::string& input,
                                                        std::size_t        index,
                                                        bool               lastTokenWasValue) const
  {
    if (lastTokenWasValue && _associativity == Associativity::Right)
      return false;

    if (!lastTokenWasValue && _associativity == Associativity::Left)
      return false;

    return input.compare(index, _strToMatch.size(), _strToMatch) == 0;
  }
}  // namespace Bistro

#define BISTRO_MAKE_UNARY_OPERATOR_TOKEN(name, op, associativity)                       \
  template <class T>                                                                    \
  class name##Token : public AUnaryOperatorToken<T, name##Value<T>>                     \
  {                                                                                     \
  public:                                                                               \
    name##Token();                                                                      \
  };                                                                                    \
                                                                                        \
  template <class T>                                                                    \
  name##Token<T>::name##Token() : AUnaryOperatorToken(op, Associativity::associativity) \
  {                                                                                     \
  }

#endif  // BISTRO_TOKENS_AUNARY_OPERATOR_TOKEN_HH_