#pragma once

#ifndef BISTRO_TOKENS_NUMBERTOKEN_HH_
#define BISTRO_TOKENS_NUMBERTOKEN_HH_

#include "Bistro/IToken.hh"
#include "Bistro/Values/NumberValue.hh"

namespace Bistro
{
  template <class T>
  class NumberToken : public IToken<T>
  {
  public:
    NumberToken();
    ~NumberToken();

  public:
    std::size_t getMinimumLength() const override;

    bool test(const std::string& input, std::size_t index, bool) const override;

    TokenType getType() const override;

    ValuePtrType parse(const std::string& input,
                       std::size_t        index,
                       std::size_t&       len) const override;

    bool handle(ResultContainerType<T>&    result,
                OperatorsContainerType<T>& operators,
                ValueType<T>&              currentValue) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T>
  NumberToken<T>::NumberToken()
  {
  }

  template <class T>
  NumberToken<T>::~NumberToken()
  {
  }

  template <class T>
  std::size_t NumberToken<T>::getMinimumLength() const
  {
    return 1;
  }

  template <class T>
  TokenType NumberToken<T>::getType() const
  {
    return TokenType::Number;
  }

  template <class T>
  bool NumberToken<T>::handle(ResultContainerType<T>&    result,
                              OperatorsContainerType<T>& operators,
                              ValueType<T>&              value) const
  {
    Bistro::Handlers::directPush(result, operators, value);
    return true;
  }

  bool NumberToken<int>::test(const std::string& input, std::size_t index, bool) const
  {
    return input.at(index) >= '0' && input.at(index) <= '9';
  }

  auto NumberToken<int>::parse(const std::string& input, std::size_t index, std::size_t& len) const
    -> ValuePtrType
  {
    ValuePtrType result(nullptr);
    int          value = 0;

    try
    {
      value = std::stoi(input.substr(index), &len);
    }
    catch (std::out_of_range&)
    {
      std::cerr << "Exception, number too big\n";
      abort();
    }

    result.reset(new NumberValue<int>(value));
    return result;
  }

  bool NumberToken<double>::test(const std::string& input, std::size_t index, bool) const
  {
    bool isValid = input.at(index) >= '0' && input.at(index) <= '9';
    isValid
      = isValid
        || (input.at(index) == '.' && input.at(index + 1) >= '0' && input.at(index + 1) <= '9');
    return isValid;
  }

  auto NumberToken<double>::parse(const std::string& input,
                                  std::size_t        index,
                                  std::size_t&       len) const -> ValuePtrType
  {
    ValuePtrType result(nullptr);
    double       value = 0;

    try
    {
      value = std::stod(input.substr(index), &len);
    }
    catch (std::out_of_range&)
    {
      std::cerr << "Exception, number too big\n";
      abort();
    }

    result.reset(new NumberValue<double>(value));
    return result;
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_NUMBERTOKEN_HH_