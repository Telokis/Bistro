#pragma once

#ifndef BISTRO_TOKENS_ABINARY_OPERATOR_TOKEN_HH_
#define BISTRO_TOKENS_ABINARY_OPERATOR_TOKEN_HH_

#include "Bistro/IToken.hh"
#include "Bistro/Tokens/AOperatorToken.hh"

namespace Bistro
{
  template <class T, class BinaryOperatorValue>
  class ABinaryOperatorToken : public AOperatorToken<T, BinaryOperatorValue>
  {
  public:
    ABinaryOperatorToken(const std::string& strToMatch);

  public:
    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  template <class T, class BinaryOperatorValue>
  ABinaryOperatorToken<T, BinaryOperatorValue>::ABinaryOperatorToken(const std::string& strToMatch)
    : AOperatorToken(strToMatch)
  {
  }

  template <class T, class BinaryOperatorValue>
  bool ABinaryOperatorToken<T, BinaryOperatorValue>::test(const std::string& input,
                                                          std::size_t        index,
                                                          bool lastTokenWasValue) const
  {
    if (!lastTokenWasValue)
      return false;
    return input.compare(index, _strToMatch.size(), _strToMatch) == 0;
  }
}  // namespace Bistro

#define BISTRO_MAKE_BINARY_OPERATOR_TOKEN(name, op)                  \
  template <class T>                                                 \
  class name##Token : public ABinaryOperatorToken<T, name##Value<T>> \
  {                                                                  \
  public:                                                            \
    name##Token();                                                   \
  };                                                                 \
                                                                     \
  template <class T>                                                 \
  name##Token<T>::name##Token() : ABinaryOperatorToken(op)           \
  {                                                                  \
  }

#endif  // BISTRO_TOKENS_ABINARY_OPERATOR_TOKEN_HH_