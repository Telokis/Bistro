#pragma once

#ifndef BISTRO_TOKENS_RIGHT_PARENTHESIS_TOKEN_HH_
#define BISTRO_TOKENS_RIGHT_PARENTHESIS_TOKEN_HH_

#include "Bistro/IToken.hh"

namespace Bistro
{
  template <class T>
  class RightParenthesisToken : public IToken<T>
  {
  public:
    RightParenthesisToken();

  public:
    std::size_t getMinimumLength() const override;

    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;

    TokenType getType() const override;

    ValuePtrType parse(const std::string& input,
                       std::size_t        index,
                       std::size_t&       len) const override;

    bool handle(ResultContainerType<T>&    result,
                OperatorsContainerType<T>& operators,
                ValueType<T>&              currentValue) const override;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

#include "Bistro/Values/RightParenthesisValue.hh"

namespace Bistro
{
  template <class T>
  RightParenthesisToken<T>::RightParenthesisToken()
  {
  }

  template <class T>
  std::size_t RightParenthesisToken<T>::getMinimumLength() const
  {
    return 1;
  }

  template <class T>
  bool RightParenthesisToken<T>::test(const std::string& input,
                                      std::size_t        index,
                                      bool               lastTokenWasValue) const
  {
    return input.at(index) == ')';
  }

  template <class T>
  TokenType RightParenthesisToken<T>::getType() const
  {
    return TokenType::RightParenthesis;
  }

  template <class T>
  bool RightParenthesisToken<T>::handle(ResultContainerType<T>&    result,
                                        OperatorsContainerType<T>& operators,
                                        ValueType<T>&              value) const
  {
    while (!operators.empty() && operators.back()->getType() != TokenType::LeftParenthesis)
    {
      result.push_back(std::move(operators.back()));
      operators.pop_back();
    }

    // No matching LeftParenthesis
    if (operators.empty())
    {
      std::cerr << "No matching left parenthesis found for right parenthesis.\n";
      return false;
    }

    operators.pop_back();

    // Handling of functions
    if (!operators.empty() && operators.back()->getType() == TokenType::Function)
    {
      result.push_back(std::move(operators.back()));
      operators.pop_back();
    }

    return true;
  }

  template <class T>
  auto RightParenthesisToken<T>::parse(const std::string& input,
                                       std::size_t        index,
                                       std::size_t&       len) const -> ValuePtrType
  {
    len = 1;
    return std::make_unique<RightParenthesisValue<T>>();
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_RIGHT_PARENTHESIS_TOKEN_HH_