#pragma once

#ifndef BISTRO_TOKENS_FUNCTIONTOKEN_HH_
#define BISTRO_TOKENS_FUNCTIONTOKEN_HH_

#include <functional>
#include "Bistro/IToken.hh"

namespace Bistro
{
  template <class T>
  class FunctionToken : public IToken<T>
  {
  public:
    using HandlerType = std::function<T(const std::vector<T>& args)>;

  public:
    template <class Func>
    FunctionToken(const std::string_view& name, const Func& handler);

  public:
    std::size_t getMinimumLength() const override;
    bool test(const std::string& input, std::size_t index, bool lastTokenWasValue) const override;
    TokenType    getType() const override;
    ValuePtrType parse(const std::string& input,
                       std::size_t        index,
                       std::size_t&       len) const override;
    bool         handle(ResultContainerType<T>&    result,
                        OperatorsContainerType<T>& operators,
                        ValueType<T>&              currentValue) const override;

  private:
    const std::string _strToMatch;
    HandlerType       _handler;
    size_t            _argsCount;
  };
}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

#include "Bistro/Values/FunctionValue.hh"
#include "Bistro/FunctionArity.hh"

namespace Bistro
{
  namespace
  {
    template <class T, class Func, size_t... Indices>
    decltype(auto) wrapHandlerImpl(const Func& handler, std::index_sequence<Indices...>)
    {
      return [&handler](const std::vector<T>& args) { return handler(args.at(Indices)...); };
    }
  };  // namespace

  template <class T>
  template <class Func>
  FunctionToken<T>::FunctionToken(const std::string_view& name, const Func& handler)
    : _strToMatch(name)
  {
    _argsCount = FunctionArity_v<Func>;
    _handler
      = wrapHandlerImpl<T>(std::move(handler), std::make_index_sequence<FunctionArity_v<Func>>{});
  }

  template <class T>
  std::size_t FunctionToken<T>::getMinimumLength() const
  {
    // We add 2 for the parenthesis
    // We add _argsCount because that the minimum number of chars for _argsCount arguments
    // We add _argsCount-1 because that's the number of commas for _argsCount arguments
    return _strToMatch.size() + 2 + _argsCount + _argsCount - 1;
  }

  template <class T>
  bool FunctionToken<T>::test(const std::string& input,
                              std::size_t        index,
                              bool               lastTokenWasValue) const
  {
    if (lastTokenWasValue)
      return false;

    size_t len = _strToMatch.size();

    if (input.compare(index, len, _strToMatch) == 0)
    {
      // We add a hard requirement for an open parenthesis
      // to be right after the name of the function
      return input.size() > len + index && input.at(len + index) == '(';
    }

    return false;
  }

  template <class T>
  TokenType FunctionToken<T>::getType() const
  {
    return TokenType::Function;
  }

  template <class T>
  bool FunctionToken<T>::handle(ResultContainerType<T>&    result,
                                OperatorsContainerType<T>& operators,
                                ValueType<T>&              value) const
  {
    // A function is considered a value but we still need to create
    // a LeftParenthesis value in the operators.
    Bistro::Handlers::simpleOperator(result, operators, value);
    operators.push_back(std::make_unique<LeftParenthesisValue<T>>());

    return true;
  }

  template <class T>
  auto FunctionToken<T>::parse(const std::string& input, std::size_t index, std::size_t& len) const
    -> ValuePtrType
  {
    len = _strToMatch.size() + 1;  // +1 Because we take the left parenthesis with u
    return std::make_unique<FunctionValue<T>>(_handler, _argsCount);
  }
}  // namespace Bistro

#endif  // BISTRO_TOKENS_FUNCTIONTOKEN_HH_