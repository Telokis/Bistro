#pragma once

#ifndef BISTRO_DEFAULT_HANDLERS_HH_
#define BISTRO_DEFAULT_HANDLERS_HH_

#include <memory>
#include <deque>

#include "Bistro/Values/AOperatorStackEntry.hh"

namespace Bistro
{
  template <class T>
  using ResultContainerType = std::deque<std::unique_ptr<IValue<T>>>;

  template <class T>
  using OperatorsContainerType = std::deque<std::unique_ptr<AOperatorStackEntry<T>>>;

  template <class T>
  using ValueType = std::unique_ptr<IValue<T>>;

  namespace Handlers
  {
    template <class T>
    void directPush(ResultContainerType<T>& result,
                    OperatorsContainerType<T>&,
                    ValueType<T>& value);

    template <class T>
    void simpleOperator(ResultContainerType<T>&    result,
                        OperatorsContainerType<T>& ops,
                        ValueType<T>&              value);
  }  // namespace Handlers

}  // namespace Bistro

//  _____         _          _  _
// |  __ \       | |        (_)| |
// | |  | |  ___ | |_  __ _  _ | |
// | |  | | / _ \| __|/ _` || || |
// | |__| ||  __/| |_| (_| || || |
// |_____/  \___| \__|\__,_||_||_|
//

namespace Bistro
{
  namespace Handlers
  {
    template <class T>
    void directPush(ResultContainerType<T>& result, OperatorsContainerType<T>&, ValueType<T>& value)
    {
      result.push_back(std::move(value));
    }

    template <class T>
    void simpleOperator(ResultContainerType<T>&    result,
                        OperatorsContainerType<T>& ops,
                        ValueType<T>&              value)
    {
      std::unique_ptr<AOperatorStackEntry<T>> op(
        static_cast<AOperatorStackEntry<T>*>(value.release()));

      while (!ops.empty() && ops.back()->getType() == TokenType::Operator)
      {
        if (!(op->getAssociativity() == Associativity::Left
              && op->getPrecedence() <= ops.back()->getPrecedence())
            && !(op->getAssociativity() == Associativity::Right
                 && op->getPrecedence() < ops.back()->getPrecedence()))
        {
          break;
        }

        result.push_back(std::move(ops.back()));
        ops.pop_back();
      }

      ops.push_back(std::move(op));
    }
  }  // namespace Handlers
}  // namespace Bistro

#endif  // BISTRO_DEFAULT_HANDLERS_HH_