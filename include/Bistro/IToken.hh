#pragma once

#ifndef BISTRO_ITOKEN_HH_
#define BISTRO_ITOKEN_HH_

#include <string>
#include <memory>
#include "Bistro/TokenType.hh"
#include "Bistro/DefaultHandlers.hh"

namespace Bistro
{
  template <class T>
  class IValue;

  template <class T>
  class IToken
  {
  public:
    using ValuePtrType = std::unique_ptr<IValue<T>>;

    virtual ~IToken(){};

    virtual std::size_t getMinimumLength() const = 0;

    virtual bool test(const std::string& input,
                      std::size_t        index,
                      bool               lastTokenWasValue) const = 0;

    virtual TokenType getType() const = 0;

    virtual ValuePtrType parse(const std::string& input,
                               std::size_t        index,
                               std::size_t&       len) const = 0;

    virtual bool handle(ResultContainerType<T>&    result,
                        OperatorsContainerType<T>& operators,
                        ValueType<T>&              currentValue) const = 0;
  };
}  // namespace Bistro

#endif  // BISTRO_ITOKEN_HH_