#pragma once

#ifndef BISTRO_FUNCTION_ARITY_HH_
#define BISTRO_FUNCTION_ARITY_HH_

#include <type_traits>

template <typename T>
struct FunctionArity : FunctionArity<decltype(&T::operator())>
{
};
template <typename R, typename... Args>
struct FunctionArity<R (*)(Args...)> : std::integral_constant<unsigned, sizeof...(Args)>
{
};
template <typename R, typename C, typename... Args>
struct FunctionArity<R (C::*)(Args...)> : std::integral_constant<unsigned, sizeof...(Args)>
{
};
template <typename R, typename C, typename... Args>
struct FunctionArity<R (C::*)(Args...) const> : std::integral_constant<unsigned, sizeof...(Args)>
{
};

template <typename T>
constexpr auto FunctionArity_v = FunctionArity<T>::value;

#endif  // BISTRO_FUNCTION_ARITY_HH_