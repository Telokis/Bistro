#pragma once

#ifndef BISTRO_ASSOCIATIVITY_HH_
#define BISTRO_ASSOCIATIVITY_HH_

namespace Bistro
{
  enum class Associativity {
    Left,
    Right,

    Any = Left
  };
}  // namespace Bistro

#endif  // BISTRO_ASSOCIATIVITY_HH_