#pragma once

#ifndef BISTRO_TOKENTYPE_HH_
#define BISTRO_TOKENTYPE_HH_

namespace Bistro
{
  enum class TokenType {
    None             = 0,
    Number           = 1 << 0,
    Variable         = 1 << 1,
    Operator         = 1 << 2,
    LeftParenthesis  = 1 << 3,
    RightParenthesis = 1 << 4,
    Function         = 1 << 5
  };

}  // namespace Bistro

#include <iostream>

std::ostream& operator<<(std::ostream& os, const Bistro::TokenType& tokenType)
{
  switch (tokenType)
  {
    case Bistro::TokenType::None: {
      return os << "None";
    }
    case Bistro::TokenType::Number: {
      return os << "Number";
    }
    case Bistro::TokenType::Variable: {
      return os << "Variable";
    }
    case Bistro::TokenType::Operator: {
      return os << "Operator";
    }
    case Bistro::TokenType::LeftParenthesis: {
      return os << "LeftParenthesis";
    }
    case Bistro::TokenType::RightParenthesis: {
      return os << "RightParenthesis";
    }
    case Bistro::TokenType::Function: {
      return os << "Function";
    }
  }

  return os;
}

#endif  // BISTRO_TOKENTYPE_HH_