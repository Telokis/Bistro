#pragma once

#ifndef BISTRO_IVALUE_HH_
#define BISTRO_IVALUE_HH_

#include <vector>

namespace Bistro
{
  template <class T>
  class IValue
  {
  public:
    virtual ~IValue(){};
    virtual std::size_t getArgumentsNumber() const                = 0;
    virtual T           process(const std::vector<T>& args) const = 0;
    virtual TokenType   getType() const                           = 0;

    virtual std::ostream& print(std::ostream& os) const
    {
      return os;
    }
  };
}  // namespace Bistro

#endif  // BISTRO_IVALUE_HH_